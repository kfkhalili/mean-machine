// load the express package and create our app
var express = require("express");
var app = express();
var path = require("path");

var port = process.env.PORT;
var ip = process.env.IP;

// send our index.html file to the user for the home page
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

// start the server
app.listen(port);
console.log(port + " is the magic port!")