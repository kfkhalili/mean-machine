// get the http and filesystem modules
var http = require("http"),
    fs = require("fs");
    
var port = process.env.PORT;
var ip = process.env.IP;

// create our server using the http module
http.createServer(function(req, res){
    
    // write to our server. set configuration for the response.
    res.writeHead(200, {
        'Content-Type': 'text/html',
        'Access-Control-Allow-Origin': '*'
    });
    
    // grab the index.html file using fs
    var readStream = fs.createReadStream(__dirname + '/index.html');
    
    // send the index.html file to our user
    readStream.pipe(res);
}).listen(port || 1337, ip || "0.0.0.0");

// tell ourselves what is happening
console.log("Visit me at http://" + ip + ":" + port);