// load the express package and create our app
var express = require("express");
var app = express();
var path = require("path");

var port = process.env.PORT;
var ip = process.env.IP;





// send our index.html file to the user for the home page
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

// create routes for the admin section

// get an instance of the router
var adminRouter = express.Router();

// route middleware that will happen on every request
adminRouter.use(function(req, res, next){
    // log each request to the console
    console.log(req.method, req.url);
    
    // continue doing what we were doing and go to the route
    next();
});

// admin main page, the dashboard (http://0.0.0.0:8080/admin)
adminRouter.get('/', function(req, res){
    res.send('I am the dashboard!');
});

// users page (http://0.0.0.0:8080/users)
adminRouter.get('/users', function(req, res){
    res.send('I show all the users!');
});

// route middleware to validate name
adminRouter.param('name', function(req, res, next, name){
    // do validation on name here
    // blah blah validation
    // log something so we know it's working
    console.log("doing some validations on " + name);
    
    // once validation is done save the new item in the req
    req.name = name;
    // go to the next thing
    next();
});

// route with parameters (http://0.0.0.0:8080/admin/users/:name)
adminRouter.get('/users/:name', function(req, res){
    res.send('hello ' + req.name + '!');
});

// posts page (http://0.0.0.0:8080/posts)
adminRouter.get('/posts', function(req, res){
    res.send('I show all the posts!');
});

// defining the login route
app.route('/login')
    // show the form (GET http://0.0.0.0:8080/login)
    .get(function(req, res){
        res.send('This is the login form');
    })
    
    // process the form (POST http://0.0.0.0:8080/login)
    .post(function(req, res){
        console.log("processing!");
        res.send('processing the login form!');
    });


// apply the routes to our application
app.use('/admin', adminRouter);

// start the server
app.listen(port);
console.log(port + " is the magic port!")